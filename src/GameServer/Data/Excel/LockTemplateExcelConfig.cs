namespace Weedwacker.GameServer.Data.Excel;

public class LockTemplateExcelConfig
{
	public string type;
	public float range;
	public float combatPri;
	public float normalPri;
}