namespace Weedwacker.GameServer.Data.Excel;

public class LevelSuppressExcelConfig
{
	public int level;
	public float levelSuppressDamageCo;
	public float levelSuppressEndure;
	public float levelSuppressDisMinHorizontal;
	public float levelSuppressDisMaxHorizontal;
	public float levelSuppressDisMinVertical;
	public float levelSuppressDisMaxVertical;
	public bool isAttackerPlayer;
	public bool isDefenserPlayer;
}