namespace Weedwacker.GameServer.Data.Excel;

public class LevelTagResetExcelConfig
{
	public uint ID;
	public uint dungeonId;
	public uint[] seriesIdList;
}