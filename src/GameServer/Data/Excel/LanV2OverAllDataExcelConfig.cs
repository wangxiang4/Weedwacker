namespace Weedwacker.GameServer.Data.Excel;

public class LanV2OverAllDataExcelConfig
{
	public uint id;
	public uint activityId;
	public uint bossDungeonId;
	public uint bossRewardId;
	public uint bossStartDay;
	public uint[] bossWatcherIdList;
	public uint[] bossExhibitionIdList;
	public uint bossPushTipsId;
	public uint[] clothesWatcherIdList;
	public uint clothesRewardId;
	public uint[] bossCardIdList;
	public uint activityPlayDuration;
	public bool hideExchangeEntry;
}