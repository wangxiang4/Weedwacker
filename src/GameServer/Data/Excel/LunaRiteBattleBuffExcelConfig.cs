using Weedwacker.GameServer.Data.Enums;

namespace Weedwacker.GameServer.Data.Excel;

public class LunaRiteBattleBuffExcelConfig
{
	public uint Id;
	public LunaRiteRegionType regionType;
	public uint number;
	public uint buffTextMapHash;
	public uint rewardId;
}