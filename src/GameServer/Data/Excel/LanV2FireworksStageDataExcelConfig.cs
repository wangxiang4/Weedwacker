namespace Weedwacker.GameServer.Data.Excel;

public class LanV2FireworksStageDataExcelConfig
{
	public uint stageId;
	public uint openDay;
	public uint[] challengeIdList;
	public uint tabNameTextMapHash;
	public uint questDescTextMapHash;
	public uint[] guideQuestId;
	public uint guideQuestRewardPreviewId;
}