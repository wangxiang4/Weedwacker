using Weedwacker.GameServer.Data.Enums;

namespace Weedwacker.GameServer.Data.Excel;

public class LanV2FireworksFactorDataExcelConfig
{
	public uint factorId;
	public uint[] perfectRange;
	public uint factorLength;
	public FireworksReformParamType type;
	public uint[] colorRange;
}