using Weedwacker.GameServer.Data.Enums;

namespace Weedwacker.GameServer.Data.Excel;

public class LunaRiteBattleExcelConfig
{
	public uint Id;
	public LunaRiteRegionType regionType;
	public uint consecrateID;
	public uint groupBundleId;
	public uint rewardID;
	public string challengeIcon;
	public string monsterInfo;
	public string eliteMonsterInfo;
	public uint rulerTextMapHash;
	public uint recipeSourceTextMapHash;
}