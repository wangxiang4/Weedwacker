namespace Weedwacker.GameServer.Data.Excel;

public class LanV2FireworksChallengeDataExcelConfig
{
	public uint challengeId;
	public uint titleTextMapHash;
	public string icon;
	public uint[] factorIdList;
	public uint initFireElementValue;
	public uint fullScore;
	public uint unlockAbilityScore;
	public uint unlockFireworksScore;
	public uint[] watcherIdList;
	public uint addStaminaValue;
}