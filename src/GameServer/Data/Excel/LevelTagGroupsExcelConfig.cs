namespace Weedwacker.GameServer.Data.Excel;

public class LevelTagGroupsExcelConfig
{
	public uint ID;
	public LevelTagGroup[] levelTagGroupList;
	public uint[] initialLevelTagIdList;
	public uint changeCd;

	public class LevelTagGroup
	{
		public uint[] levelTagIdList;
	}
}