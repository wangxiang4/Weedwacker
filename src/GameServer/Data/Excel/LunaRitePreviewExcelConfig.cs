namespace Weedwacker.GameServer.Data.Excel;

public class LunaRitePreviewExcelConfig
{
	public uint Id;
	public uint activityId;
	public uint unlockQuestId;
	public uint unlockQuestId2;
	public uint unlockPlayerLevel;
	public uint rewardId;
	public uint activityQuestId;
	public uint guideQuestId;
	public uint challengePushTipsID;
	public uint plotPushTipsID;
	public uint plotPushTipsPreQuestID;
	public uint maxAtmosphere;
}