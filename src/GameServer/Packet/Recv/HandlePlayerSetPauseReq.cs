﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.PlayerSetPauseReq)]
	public static async Task HandlePlayerSetPauseReq(Connection session, byte[] header, byte[] payload)
	{
		PacketHead head = PacketHead.Parser.ParseFrom(header);
		PlayerSetPauseReq req = PlayerSetPauseReq.Parser.ParseFrom(payload);

		await session.SendPacketAsync(new PacketPlayerSetPauseRsp(head.ClientSequenceId));
		session.Player.SetPaused(req.IsPaused);
	}
}
