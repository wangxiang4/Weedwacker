﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.AvatarDieAnimationEndReq)]
	public static async Task HandleAvatarDieAnimationEndReq(Connection session, byte[] header, byte[] payload)
	{
		AvatarDieAnimationEndReq req = AvatarDieAnimationEndReq.Parser.ParseFrom(payload);

		await session.Player.TeamManager.OnAvatarDie((long)req.DieGuid);

		await session.SendPacketAsync(new PacketAvatarDieAnimationEndRsp(req.DieGuid, 0));
	}
}
