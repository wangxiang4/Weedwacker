﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.AvatarPromoteReq)]
	public static async Task HandleAvatarPromoteReq(Connection session, byte[] header, byte[] payload)
	{
		AvatarPromoteReq req = AvatarPromoteReq.Parser.ParseFrom(payload);
		await session.Player.Avatars.PromoteAvatarAsync(req.Guid);
		await session.Player.SendPacketAsync(new PacketAvatarPromoteRsp(req.Guid));
	}
}
