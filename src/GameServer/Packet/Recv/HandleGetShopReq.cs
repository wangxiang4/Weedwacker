﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.GetShopReq)]
	public static async Task HandleGetShopReq(Connection session, byte[] header, byte[] payload)
	{
		GetShopReq req = GetShopReq.Parser.ParseFrom(payload);
		await session.SendPacketAsync(new PacketGetShopRsp(session.Player, req.ShopType));
	}
}
