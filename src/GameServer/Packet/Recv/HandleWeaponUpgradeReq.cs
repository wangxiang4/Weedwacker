﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.GameServer.Systems.Inventory;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.WeaponUpgradeReq)]
	public static async Task HandleWeaponUpgradeReq(Connection session, byte[] header, byte[] payload)
	{
		WeaponUpgradeReq req = WeaponUpgradeReq.Parser.ParseFrom(payload);
		List<ItemParam> leftoverOres = new(); //TODO
		uint oldLevel = (session.Player.Inventory.GuidMap[req.TargetWeaponGuid] as WeaponItem).Level;
		WeaponItem weapon = await session.Player.Inventory.UpgradeWeaponAsync(req.TargetWeaponGuid, req.FoodWeaponGuidList.ToList(), req.ItemParamList.ToList());
		await session.Player.SendPacketAsync(new PacketWeaponUpgradeRsp(weapon, oldLevel, leftoverOres));
	}
}

