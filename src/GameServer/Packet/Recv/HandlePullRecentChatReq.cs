﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.PullRecentChatReq)]
	public static async Task HandlePullRecentChatReq(Connection session, byte[] header, byte[] payload)
	{
		//TODO
		await session.SendPacketAsync(new PacketPullRecentChatRsp());
	}
}
