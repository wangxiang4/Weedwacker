﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.CalcWeaponUpgradeReturnItemsReq)]
	public static async Task HandleCalcWeaponUpgradeReturnItemsReq(Connection session, byte[] header, byte[] payload)
	{
		CalcWeaponUpgradeReturnItemsReq req = CalcWeaponUpgradeReturnItemsReq.Parser.ParseFrom(payload);
		List<ItemParam> returnOres = new(); //TODO
		await session.SendPacketAsync(new PacketCalcWeaponUpgradeReturnItemsRsp(req.TargetWeaponGuid, returnOres));
	}
}

