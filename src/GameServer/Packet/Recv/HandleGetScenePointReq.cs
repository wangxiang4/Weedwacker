﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.GetScenePointReq)]
	public static async Task HandleGetScenePointReq(Connection session, byte[] header, byte[] payload)
	{
		GetScenePointReq req = GetScenePointReq.Parser.ParseFrom(payload);
		await session.SendPacketAsync(new PacketGetScenePointRsp(session.Player, req.SceneId));
	}
}
