﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.GetPlayerFriendListReq)]
	public static async Task HandleGetPlayerFriendListReq(Connection session, byte[] header, byte[] payload)
	{
		await session.SendPacketAsync(new PacketGetPlayerFriendListRsp(session.Player));
	}
}
