﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.SetUpAvatarTeamReq)]
	public static async Task HandleSetUpAvatarTeamReq(Connection session, byte[] header, byte[] payload)
	{
		SetUpAvatarTeamReq req = SetUpAvatarTeamReq.Parser.ParseFrom(payload);

		await session.Player.TeamManager.SetupAvatarTeamAsync((int)req.TeamId, req.AvatarTeamGuidList);
		await session.Player.SendPacketAsync(new PacketSetUpAvatarTeamRsp(session.Player, req.TeamId, req.AvatarTeamGuidList));
	}
}
