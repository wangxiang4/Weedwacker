﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.GetSceneAreaReq)]
	public static async Task HandleGetSceneAreaReq(Connection session, byte[] header, byte[] payload)
	{
		GetSceneAreaReq req = GetSceneAreaReq.Parser.ParseFrom(payload);
		await session.SendPacketAsync(new PacketGetSceneAreaRsp(session.Player, req.SceneId));
	}
}
