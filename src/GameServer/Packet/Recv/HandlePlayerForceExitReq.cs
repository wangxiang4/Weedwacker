﻿using Weedwacker.GameServer.Enums;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.PlayerForceExitReq)]
	public static async Task HandlePlayerForceExitReq(Connection session, byte[] header, byte[] payload)
	{
		await session.SendPacketAsync(new BasePacket(OpCode.PlayerForceExitRsp));
		session.Stop();
	}
}
