﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.UnlockTransPointReq)]
	public static async Task HandleUnlockTransPointReq(Connection session, byte[] header, byte[] payload)
	{
		UnlockTransPointReq req = UnlockTransPointReq.Parser.ParseFrom(payload);
		bool unlocked = await session.Player.ProgressManager.UnlockTransPoint((int)req.SceneId, (int)req.PointId);
		await session.SendPacketAsync(new PacketUnlockTransPointRsp(unlocked ? Retcode.RetSucc : Retcode.RetFail));
	}
}
