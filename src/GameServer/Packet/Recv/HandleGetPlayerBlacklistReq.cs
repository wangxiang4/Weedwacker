﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.GetPlayerBlacklistReq)]
	public static async Task HandleGetPlayerBlacklistReq(Connection session, byte[] header, byte[] payload)
	{
		await session.SendPacketAsync(new PacketGetPlayerBlacklistRsp(session.Player));
	}
}
