﻿using Weedwacker.GameServer.Enums;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.MonsterAIConfigHashNotify)]
	public static async Task HandleMonsterAIConfigHashNotify(Connection session, byte[] header, byte[] payload)
	{
		// TODO: Handle MonsterAIConfigHashNotify
	}
}
