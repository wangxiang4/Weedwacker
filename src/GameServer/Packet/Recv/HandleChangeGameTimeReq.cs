﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.ChangeGameTimeReq)]
	public static async Task HandleChangeGameTimeReq(Connection session, byte[] header, byte[] payload)
	{
		ChangeGameTimeReq req = ChangeGameTimeReq.Parser.ParseFrom(payload);
		session.Player.Scene.ChangeTime((int)req.GameTime);
		session.SendPacketAsync(new PacketChangeGameTimeRsp(session.Player));
	}
}
