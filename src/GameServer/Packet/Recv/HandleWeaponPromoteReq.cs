﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.GameServer.Systems.Inventory;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.WeaponPromoteReq)]
	public static async Task HandleWeaponPromoteReq(Connection session, byte[] header, byte[] payload)
	{
		WeaponPromoteReq req = WeaponPromoteReq.Parser.ParseFrom(payload);
		uint oldPromote = (session.Player.Inventory.GuidMap[req.TargetWeaponGuid] as WeaponItem).PromoteLevel;
		WeaponItem weapon = await session.Player.Inventory.PromoteWeaponAsync(req.TargetWeaponGuid);
		await session.Player.SendPacketAsync(new PacketWeaponPromoteRsp(weapon, oldPromote));
	}
}

