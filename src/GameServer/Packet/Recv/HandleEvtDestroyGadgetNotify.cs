﻿using Weedwacker.GameServer.Enums;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.EvtDestroyGadgetNotify)]
	public static async Task HandleEvtDestroyGadgetNotify(Connection session, byte[] header, byte[] payload)
	{
		EvtDestroyGadgetNotify req = EvtDestroyGadgetNotify.Parser.ParseFrom(payload);

		await session.Player.Scene.OnPlayerDestroyGadget(req.EntityId);
	}
}
